1
99:59:59,999 --> 99:59:59,999
let's welcome wookey

2
99:59:59,999 --> 99:59:59,999
he's talking about infrastructure updates

3
99:59:59,999 --> 99:59:59,999
and whether we can change anything in less than two years

4
99:59:59,999 --> 99:59:59,999
i hope so

5
99:59:59,999 --> 99:59:59,999
wookie: ok, can everybody hear me?

6
99:59:59,999 --> 99:59:59,999
well, so, thank you for coming back after dinner not just going back to the pub

7
99:59:59,999 --> 99:59:59,999
i'm impressed by your enthusiasm

8
99:59:59,999 --> 99:59:59,999
so, yes

9
99:59:59,999 --> 99:59:59,999
we have this interesting problem

10
99:59:59,999 --> 99:59:59,999
that it's quite hard to change things quickly in Debian

11
99:59:59,999 --> 99:59:59,999
now we already knew that

12
99:59:59,999 --> 99:59:59,999
but i just want to talk a little bit about

13
99:59:59,999 --> 99:59:59,999
some experience i've had in

14
99:59:59,999 --> 99:59:59,999
last few years trying to change a particular thing

15
99:59:59,999 --> 99:59:59,999
which has not gone particularly well

16
99:59:59,999 --> 99:59:59,999
i would love to think we could have been a bit better

17
99:59:59,999 --> 99:59:59,999
so i really do want to talk about the general problem here of

18
99:59:59,999 --> 99:59:59,999
the way our infrastructure work and how if we miss a stable release

19
99:59:59,999 --> 99:59:59,999
that can be a real problem

20
99:59:59,999 --> 99:59:59,999
but i shall use this example

21
99:59:59,999 --> 99:59:59,999
of build profiles just because that's happened recently

22
99:59:59,999 --> 99:59:59,999
and i think it illustrates the point

23
99:59:59,999 --> 99:59:59,999
or at least some points

24
99:59:59,999 --> 99:59:59,999
I'd very much like to discuss things rather than blither on for too long

25
99:59:59,999 --> 99:59:59,999
so I try not to blither on for too long

26
99:59:59,999 --> 99:59:59,999
so

27
99:59:59,999 --> 99:59:59,999
just to clarfiy

28
99:59:59,999 --> 99:59:59,999
in case anybody is not clear

29
99:59:59,999 --> 99:59:59,999
all of our infrastructure basically runs on stable

30
99:59:59,999 --> 99:59:59,999
there are a few exceptions to that

31
99:59:59,999 --> 99:59:59,999
for practical reasons

32
99:59:59,999 --> 99:59:59,999
so if it is not in stable, you cannot use it in the infrastructure

33
99:59:59,999 --> 99:59:59,999
so there are ??
