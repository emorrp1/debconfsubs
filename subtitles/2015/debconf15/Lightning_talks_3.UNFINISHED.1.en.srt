1
00:32:10,280 --> 00:32:16,120
[applause]

2
00:32:16,120 --> 00:32:21,720
Thank you very much.
Next step is Stefan Weil

3
00:32:22,380 --> 00:32:26,480
talking about PalMA and Debian
in Mannheim.

4
00:32:58,960 --> 00:33:02,400
Thanks. Hello and welcome everybody.
My name is Stefan Weil,

5
00:33:02,420 --> 00:33:02,670
and I am going to tell you about a 
Debian based freeware development

6
00:33:07,281 --> 00:33:09,181
at Mannheim University Library.

7
00:33:09,181 --> 00:33:14,780
Large parts of our library are located in
Mannheim Palace,

8
00:33:14,780 --> 00:33:18,521
and there we transformed a former reading
room into a modern learning center

9
00:33:18,521 --> 00:33:21,740
which opened in spring last year.

10
00:33:21,740 --> 00:33:26,880
This learning center provides different
kinds of places for working in groups,

11
00:33:26,880 --> 00:33:31,160
and most of them have a large monitor
where the group members can share

12
00:33:31,160 --> 00:33:34,621
their presentations and any documents.

13
00:33:34,621 --> 00:33:41,020
Students bring their own devices -
laptops, smartphones or tablet computers.

14
00:33:41,020 --> 00:33:45,381
They should be able to use the team
monitors by wireless LAN,

15
00:33:45,381 --> 00:33:48,260
without any cables.

16
00:33:48,260 --> 00:33:51,400
Existing solutions did not match
our requirements,

17
00:33:51,400 --> 00:34:01,120
so we wrapped existing technology in a new
web application which we called PalMA.

18
00:34:01,120 --> 00:34:06,660
The team monitor shows some short
instructions how to use PalMA

19
00:34:06,660 --> 00:34:13,700
(which is an abbreviation of
"Present and learn in Mannheim").

20
00:34:13,700 --> 00:34:18,660
Other libraries which adapted PalMA later
have chosen a different name like

21
00:34:18,660 --> 00:34:26,400
SPrinT (which stands for
"Study and present in teams").

22
00:34:26,400 --> 00:34:32,400
As soon as a user connects to the
PalMA URL, he or she gets a web interface

23
00:34:32,400 --> 00:34:37,540
which controls all aspects of PalMA:

24
00:34:37,540 --> 00:34:41,240
This is what the user sees in the browser,

25
00:34:41,240 --> 00:34:45,141
and in the upper right corner, you
see the connected group members,

26
00:34:45,141 --> 00:34:53,840
then the user can show web pages
in the left lower corner

27
00:34:53,840 --> 00:34:56,500
on the team monitor.

28
00:34:56,500 --> 00:35:01,560
He can upload different kinds of files
(also in the left corner) for displaying

29
00:35:01,560 --> 00:35:04,360
on the team monitor, or he can use

30
00:35:04,360 --> 00:35:10,760
screen mirroring of the local display
on the team monitor.

31
00:35:10,760 --> 00:35:18,281
The team monitor can show up to
4 different windows at a time

32
00:35:18,281 --> 00:35:21,281
by changing the screen layout.

33
00:35:21,281 --> 00:35:27,981
You see this in the left part:
there are 5 different screen layouts.

34
00:35:27,981 --> 00:35:32,140
Each window supports scrolling and
zooming and other operations

35
00:35:32,140 --> 00:35:36,720
via the web interface.

36
00:35:36,720 --> 00:35:39,780
The web user interface is available
in several languages,

37
00:35:39,780 --> 00:35:43,820
most of them were contributed
by students of our university.

38
00:35:51,340 --> 00:35:52,781
Each PalMA monitor is controlled by a
Debian GNU Linux system

39
00:35:54,800 --> 00:35:57,301
running a display server, a window
manager, web server and different kinds

40
00:35:57,860 --> 00:36:02,020
of viewer applications which are
started on demand.

41
00:36:02,020 --> 00:36:11,400
The user runs a web browser and optionally
a VNC server for screen mirroring. Today,

42
00:36:11,400 --> 00:36:19,360
PalMA is usable in a trusted environment
like a university or private network.

43
00:36:19,360 --> 00:36:25,720
At least 7 libraries currently use it
for group working places.

44
00:36:25,720 --> 00:36:29,680
Nevertheless we believe that PalMA
can be improved further

45
00:36:29,680 --> 00:36:39,600
and that there are more useful
applications for PalMA.

46
00:36:39,600 --> 00:36:44,640
You can find more information
on our website and also on GitHub.

47
00:36:44,640 --> 00:36:51,300
And I'd be happy to get your feedback or
new contributions for PalMA.

48
00:36:51,300 --> 00:36:55,341
Maybe there are also Debian developers
who are willing to support special

49
00:36:55,341 --> 00:36:57,300
software for libraries.

50
00:36:57,300 --> 00:36:59,740
In any case, just drop me an e-mail.

51
00:36:59,740 --> 00:37:02,401
Thanks for your attention.

52
00:37:02,401 --> 00:37:10,020
[applause]
