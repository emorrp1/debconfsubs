1
99:59:59,999 --> 99:59:59,999
welcome everyone

2
99:59:59,999 --> 99:59:59,999
so this is the git-buildpackage skillshare BOF

3
99:59:59,999 --> 99:59:59,999
and im hoping to take some notes here

4
99:59:59,999 --> 99:59:59,999
im here basically as a facilitator

5
99:59:59,999 --> 99:59:59,999
i dont plan to tell you exactly what you should do

6
99:59:59,999 --> 99:59:59,999
what im hoping to do is to learn from everyone here

7
99:59:59,999 --> 99:59:59,999
and we can all mutually get some practices ??

8
99:59:59,999 --> 99:59:59,999
maybe to get a sense of the room initally

9
99:59:59,999 --> 99:59:59,999
so we all know who we are talking to

10
99:59:59,999 --> 99:59:59,999
who here is currently maintaining a debian package

11
99:59:59,999 --> 99:59:59,999
ok, and who here is thinking he might want to maintain

12
99:59:59,999 --> 99:59:59,999
a debian package in the future but is not currently

13
99:59:59,999 --> 99:59:59,999
maintaining one now

14
99:59:59,999 --> 99:59:59,999
alright, cool, awesome

15
99:59:59,999 --> 99:59:59,999
i really glad to have folks who are starting packaging

16
99:59:59,999 --> 99:59:59,999
one now and are starting thinking how this workflow goes

17
99:59:59,999 --> 99:59:59,999
?? this people were just only packaging

18
99:59:59,999 --> 99:59:59,999
so, of the peopl who are here packaging

19
99:59:59,999 --> 99:59:59,999
how many people currently use git-buildpackage

20
99:59:59,999 --> 99:59:59,999
but thats not everyone that currently packages

21
99:59:59,999 --> 99:59:59,999
so its great that people are thinking about this

22
99:59:59,999 --> 99:59:59,999
so if people have other packaging schemas that use version control

23
99:59:59,999 --> 99:59:59,999
that they are currently using and that they are thinking of

24
99:59:59,999 --> 99:59:59,999
??

25
99:59:59,999 --> 99:59:59,999
or that are here just to troll us because your system is better

26
99:59:59,999 --> 99:59:59,999
that is great and i want to hear what works for you and

27
99:59:59,999 --> 99:59:59,999
other systems and workflows that

28
99:59:59,999 --> 99:59:59,999
?? point out analogies of things that work without git-buildpackages

29
99:59:59,999 --> 99:59:59,999
or other tools that we can share and ??

30
99:59:59,999 --> 99:59:59,999
so ?? remain me... how many minutes this session is?

31
99:59:59,999 --> 99:59:59,999
is it a 45 minutes session? do i hear 30?

32
99:59:59,999 --> 99:59:59,999
I wonder if somebody want to take a crack at explaining

33
99:59:59,999 --> 99:59:59,999
to people in the room who maybe dont yet use git-buildpackage

34
99:59:59,999 --> 99:59:59,999
just briefly, if you could just try to do it in 2 sentences

35
99:59:59,999 --> 99:59:59,999
what git-buildpackage means to you as a packager

36
99:59:59,999 --> 99:59:59,999
what does it do for you as a maintainer

37
99:59:59,999 --> 99:59:59,999
what is the thing that... pick a highlight, a short highlight

38
99:59:59,999 --> 99:59:59,999
anyone want to volunteer to do that? someone that is currently using git-buildpackage

39
99:59:59,999 --> 99:59:59,999
what does it do for you

40
99:59:59,999 --> 99:59:59,999
- it ties tags together where either sbuilder or cowbuilder in a logical sense

41
99:59:59,999 --> 99:59:59,999
- ok, so you have either sbuilder or cowbuilder integration with git-buildpackage

42
99:59:59,999 --> 99:59:59,999
by the way, this is a path, i dont know if you can see this URL up here

43
99:59:59,999 --> 99:59:59,999
i dont know how to embigger this part of my screen

44
99:59:59,999 --> 99:59:59,999
but it is pad.riseup.net/p/ oh... there you go

45
99:59:59,999 --> 99:59:59,999
anyway, hopefully folks have seen that and you can get it from ??

46
99:59:59,999 --> 99:59:59,999
I welcome other people to take notes because im going to miss

47
99:59:59,999 --> 99:59:59,999
so you use sbuilder or cowbuilder with git-tags integration, right?

48
99:59:59,999 --> 99:59:59,999
yeah, the main value for me is that it utilize??

49
99:59:59,999 --> 99:59:59,999
not to keep track of files on my own, right?

50
99:59:59,999 --> 99:59:59,999
I can point tags for upstream for the package i want to build

51
99:59:59,999 --> 99:59:59,999
ok, so somebody else?

52
99:59:59,999 --> 99:59:59,999
- it takes care that i dont build packages with uncommited changes

53
99:59:59,999 --> 99:59:59,999
so take the wrong thing, for example, that can happen

54
99:59:59,999 --> 99:59:59,999
and that i wanted explicetly

55
99:59:59,999 --> 99:59:59,999
- ok, great

56
99:59:59,999 --> 99:59:59,999
- it builds ?? of your original tarballs if you are generating for snapshots

57
99:59:59,999 --> 99:59:59,999
a repository includes the git understandable trees within your version

58
99:59:59,999 --> 99:59:59,999
and it just generates ?? for you

59
99:59:59,999 --> 99:59:59,999
- it cares about the pristine tar handeling for me, it cares about running uscan for me

60
99:59:59,999 --> 99:59:59,999
- can you explain what pristine tar is people who dont know what it is?

61
99:59:59,999 --> 99:59:59,999
- well, pristine tar saves a minimal set of data that

62
99:59:59,999 --> 99:59:59,999
with the content of the git repository plus the delta, you can

63
99:59:59,999 --> 99:59:59,999
reproduce bitwise orig tarball

64
99:59:59,999 --> 99:59:59,999
- ok, and the reason we care about that is becauase

65
99:59:59,999 --> 99:59:59,999
debian, their whole infrastructure is framed around upstream tarballs

66
99:59:59,999 --> 99:59:59,999
whether thats how upstream distributes their code or not,

67
99:59:59,999 --> 99:59:59,999
thats the way we think about upstream source code

68
99:59:59,999 --> 99:59:59,999
so pristine tar says, we get this tarball from somewhere

69
99:59:59,999 --> 99:59:59,999
and thats exactly how to get that ??

70
99:59:59,999 --> 99:59:59,999
so how many use uscan integration?

71
99:59:59,999 --> 99:59:59,999
do you want to expalin how you do uscan integration

72
99:59:59,999 --> 99:59:59,999
- import-orig, the sub command, has this option --uscan

73
99:59:59,999 --> 99:59:59,999
- yeah, its already there

74
99:59:59,999 --> 99:59:59,999
so uscan looks at the debian's watch file and looks for new versions of the

75
99:59:59,999 --> 99:59:59,999
upstream source code via https or http or whatever

76
99:59:59,999 --> 99:59:59,999
??

77
99:59:59,999 --> 99:59:59,999
so just for people that havent maybe look at gbp before

78
99:59:59,999 --> 99:59:59,999
the way you invoke it these days, you use gbp, thats git build package

79
99:59:59,999 --> 99:59:59,999
and then you have sub command, this is very similar to git

80
99:59:59,999 --> 99:59:59,999
where you have a command and then a subcommand

81
99:59:59,999 --> 99:59:59,999
so there is gbp import-orig that says get upstream tarball and bring it into the repository

82
99:59:59,999 --> 99:59:59,999
and put it in the repository in the gbp way

83
99:59:59,999 --> 99:59:59,999
and then we have this other option that says

84
99:59:59,999 --> 99:59:59,999
it goes automatically and fetches from the network

85
99:59:59,999 --> 99:59:59,999
and also does the import-orig

86
99:59:59,999 --> 99:59:59,999
theres some other things that gbp can do besides import-orig

87
99:59:59,999 --> 99:59:59,999
build packages is another big one, so you say gbp buildpackage

88
99:59:59,999 --> 99:59:59,999
and it does some of the checks that people where talking about to make sure

89
99:59:59,999 --> 99:59:59,999
no additional files get mixed in ?? or changes that you werent expecting and got mixed in

90
99:59:59,999 --> 99:59:59,999
it requires of course than when you are using git for packaging, you are paying attention to

91
99:59:59,999 --> 99:59:59,999
what you are commited, if you go ahead and blindnly commited everything ??

92
99:59:59,999 --> 99:59:59,999
- I like to mention the third important

93
99:59:59,999 --> 99:59:59,999
- alright, this is the longes phrase i have ever heard

94
99:59:59,999 --> 99:59:59,999
- where you can also import-dsc which import a complete debian source package

95
99:59:59,999 --> 99:59:59,999
for example, if you take a package from someone else who hasnt use git before for packaging

96
99:59:59,999 --> 99:59:59,999
you can import his source package and ?? your packaging with it

97
99:59:59,999 --> 99:59:59,999
you can also import multiple with dscs and that one has a nice option to your snapsshot.d.o

98
99:59:59,999 --> 99:59:59,999
--debsnap and you get push the package name and it pushes the whole history of that package

99
99:59:59,999 --> 99:59:59,999
in your fresh to be created repository for packigng

100
99:59:59,999 --> 99:59:59,999
if i have to take over some legacy package

101
99:59:59,999 --> 99:59:59,999
- ok, this is exactly why i wanted to take this discussion

102
99:59:59,999 --> 99:59:59,999
I had no idea ?? which is great

103
99:59:59,999 --> 99:59:59,999
- they call it RTFM, right?

104
99:59:59,999 --> 99:59:59,999
- so maybe that was only new to me and everybody already knew it

105
99:59:59,999 --> 99:59:59,999
this whole sessions is teach dkg sesion. Anything else you want to teach me?

106
99:59:59,999 --> 99:59:59,999
Anybody want to sum up how gbp works for them, what things does

107
99:59:59,999 --> 99:59:59,999
Do you want to step in some workflows that maybe look at gbp ??

108
99:59:59,999 --> 99:59:59,999
?? maybe some problems you have encountered

109
99:59:59,999 --> 99:59:59,999
you are in your day to day work ??

110
99:59:59,999 --> 99:59:59,999
now and then we have a problem with
the import of the upstream tarballs

111
99:59:59,999 --> 99:59:59,999
because git goes crazy and ??

112
99:59:59,999 --> 99:59:59,999
- its php
- it might be, but its only how import, so

113
99:59:59,999 --> 99:59:59,999
you cant blame the language,
not this time at least

114
99:59:59,999 --> 99:59:59,999
so what it is the problem with it?
you import the upstream tarball

115
99:59:59,999 --> 99:59:59,999
- ?? for me

116
99:59:59,999 --> 99:59:59,999
it just gives a problem and versions conflict

117
99:59:59,999 --> 99:59:59,999
and it doesnt want to apply all the changes

118
99:59:59,999 --> 99:59:59,999
maybe something we do wrong

119
99:59:59,999 --> 99:59:59,999
??

120
99:59:59,999 --> 99:59:59,999
dkg: Im embarrassed to say that
i dont know who is on the team

121
99:59:59,999 --> 99:59:59,999
that is maintaining gbp. Are they here?

122
99:59:59,999 --> 99:59:59,999
[laughs]

123
99:59:59,999 --> 99:59:59,999
thank you very much.

124
99:59:59,999 --> 99:59:59,999
Do you know about this issue
with the php team

125
99:59:59,999 --> 99:59:59,999
- no, i dont but if you have a weird history

126
99:59:59,999 --> 99:59:59,999
theres a new thing that is merge ??
that doesnt care about history

127
99:59:59,999 --> 99:59:59,999
it just download the whole upstream tree
into the debian branch

128
99:59:59,999 --> 99:59:59,999
but it will be keeping the debian ??

129
99:59:59,999 --> 99:59:59,999
so you cant have any merge issues
- okay

130
99:59:59,999 --> 99:59:59,999
- I had this long standing problem with
