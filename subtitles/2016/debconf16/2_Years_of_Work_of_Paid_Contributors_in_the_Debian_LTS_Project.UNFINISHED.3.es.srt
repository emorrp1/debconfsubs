1
00:00:00,000 --> 00:00:01,241
2 años de contribuciones remuneradas 
en el proyecto Debian LTS

2
00:00:03,615 --> 00:00:04,912
Bienvenidos a la segunda sesión 
del día de hoy

3
00:00:05,978 --> 00:00:07,759
Esta sesión es acerca de los dos años de
contribuciones remuneradas...

4
00:00:09,359 --> 00:00:12,412
... en el proyecto Debian LTS

5
00:00:13,775 --> 00:00:17,307
Raphael estará hablando acerca de su 
experiencia, así que disfruten.

6
00:00:18,554 --> 00:00:21,561
[Aplausos]

7
00:00:21,952 --> 00:00:22,882
[Raphael]: Gracias.

8
00:00:24,816 --> 00:00:27,938
Voy a hacer una presentación del proyecto
Debian LTS.

9
00:00:28,703 --> 00:00:30,974
Hice una presentación similar el año
anterior.
