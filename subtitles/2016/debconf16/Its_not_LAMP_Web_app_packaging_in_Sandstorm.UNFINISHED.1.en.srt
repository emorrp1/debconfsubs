1
99:59:59,999 --> 99:59:59,999
Okay, hi everyone!

2
99:59:59,999 --> 99:59:59,999
hows the audio on the video thing?
great!

3
99:59:59,999 --> 99:59:59,999
hello everyone
Im surrounded by my slides, this is great!

4
99:59:59,999 --> 99:59:59,999
Thanks for coming to this talk and thanks
to ??? for letting me borrow this laptop

5
99:59:59,999 --> 99:59:59,999
while mine crashed on two monitors
because it uses the cool new intel chipset

6
99:59:59,999 --> 99:59:59,999
that isnt fully supported

7
99:59:59,999 --> 99:59:59,999
im goint to talk about sandstorm

8
99:59:59,999 --> 99:59:59,999
im building sandstorm with a team
of other people

9
99:59:59,999 --> 99:59:59,999
and its intended to be a viable alternative to
software as a service as a whole

10
99:59:59,999 --> 99:59:59,999
thats a pretty big goal to compete with
software as a service as a category

11
99:59:59,999 --> 99:59:59,999
so our first goal is to make something
about as easy to use as google docs

12
99:59:59,999 --> 99:59:59,999
but much more private

13
99:59:59,999 --> 99:59:59,999
so this talk is both about
what sandstorm is

14
99:59:59,999 --> 99:59:59,999
and how packages work inside sandstorm

15
99:59:59,999 --> 99:59:59,999
i think i will have a bunch of
spare time at the end

16
99:59:59,999 --> 99:59:59,999
then i really ask people what they want me
to dig into in more depth

17
99:59:59,999 --> 99:59:59,999
when ??? and ???
were initially working

18
99:59:59,999 --> 99:59:59,999
on building sandstorm

19
99:59:59,999 --> 99:59:59,999
they had a few principles they were
working from

20
99:59:59,999 --> 99:59:59,999
they noticed that many people
are very comfortable with using

21
99:59:59,999 --> 99:59:59,999
apps that run on web browsers

22
99:59:59,999 --> 99:59:59,999
where sandstorm is really targeting
people who have probably

23
99:59:59,999 --> 99:59:59,999
never seen a terminal in their lives

24
99:59:59,999 --> 99:59:59,999
and if they did, it was maybe for a minute
that a friend some them something

25
99:59:59,999 --> 99:59:59,999
sandstorm is targeting people who
are using webapps inside web browsers

26
99:59:59,999 --> 99:59:59,999
another principle is that people really
like to choose the software they use

27
99:59:59,999 --> 99:59:59,999
this is something that really struck me
as almost great about debian

28
99:59:59,999 --> 99:59:59,999
which is, in one hand is a huge amount
of software available on the debian archive

29
99:59:59,999 --> 99:59:59,999
and in the other hand you need to be
root to install any of it

30
99:59:59,999 --> 99:59:59,999
this is a real conflict with the fact that
i run a share server for some friends

31
99:59:59,999 --> 99:59:59,999
and that whenever they want any software

32
99:59:59,999 --> 99:59:59,999
sure, all of our software in debian is free,
but the needed to ask me

33
99:59:59,999 --> 99:59:59,999
permission to install it

34
99:59:59,999 --> 99:59:59,999
we liked to build something where people
can install whatever software

35
99:59:59,999 --> 99:59:59,999
they want to use saftely

36
99:59:59,999 --> 99:59:59,999
the other principle sandstorm is based on
is the idea that security sandboxing

37
99:59:59,999 --> 99:59:59,999
is an achievable goal

38
99:59:59,999 --> 99:59:59,999
im goint to start this talk with a quick
tour of sandstorm and then talk

39
99:59:59,999 --> 99:59:59,999
a few more things in depth and then
conclude with a few comparisons

40
99:59:59,999 --> 99:59:59,999
we will start with this tour and i will
you what sandstorm looks like

41
99:59:59,999 --> 99:59:59,999
to users

42
99:59:59,999 --> 99:59:59,999
i will show you what happens on that demo

43
99:59:59,999 --> 99:59:59,999
and then ill demo how to package
some software
