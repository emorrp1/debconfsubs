1
99:59:59,999 --> 99:59:59,999
It's one-thirty and we have a number
of people who put their name down

2
99:59:59,999 --> 99:59:59,999
for lightning talks, so let's begin.

3
99:59:59,999 --> 99:59:59,999
So, the rules are simple:
five minutes and three slides, no more.

4
99:59:59,999 --> 99:59:59,999
We actually have a gap for the first session
so we may as well skip straight over:

5
99:59:59,999 --> 99:59:59,999
Ian.

6
99:59:59,999 --> 99:59:59,999
[ Negotiating the projector. ]

7
99:59:59,999 --> 99:59:59,999
Ok, so I'm here to plug 'dgit',
which is my baby -

8
99:59:59,999 --> 99:59:59,999
everybody should be using 'dgit'.

9
99:59:59,999 --> 99:59:59,999
Well not quite everyone, but we'll see.

10
99:59:59,999 --> 99:59:59,999
Put your hand up if you're
comfortable with 'git'.

11
99:59:59,999 --> 99:59:59,999
Excellent, that's pretty good.
Keep your hand up if you do

12
99:59:59,999 --> 99:59:59,999
bug-squashing NMUs.

13
99:59:59,999 --> 99:59:59,999
Right, you should be using 'git' for that.
You should be using 'dgit'

14
99:59:59,999 --> 99:59:59,999
- you can put your hands down now -

15
99:59:59,999 --> 99:59:59,999
if you use 'dgit' you will be able
to do everything in 'git',

16
99:59:59,999 --> 99:59:59,999
you will never need
to read another 'debdiff',

17
99:59:59,999 --> 99:59:59,999
you can use 'git diff' and 'dgit'
will make sure that what you upload

18
99:59:59,999 --> 99:59:59,999
is what you meant to upload.

19
99:59:59,999 --> 99:59:59,999
You don't have to care about
maintainers with poor 'git' workflows

20
99:59:59,999 --> 99:59:59,999
or their weird 'git' trees

21
99:59:59,999 --> 99:59:59,999
You can just do everything in 'git'.

22
99:59:59,999 --> 99:59:59,999
You can cherry-pick your upstream
bugfix directly from the upstream branch

23
99:59:59,999 --> 99:59:59,999
onto what 'dgit' gives you.

24
99:59:59,999 --> 99:59:59,999
Fix up the conflicts, 'dgit push',
the job is done.

25
99:59:59,999 --> 99:59:59,999
You can 'dgit push' to 'DELAYED'
as well now.

26
99:59:59,999 --> 99:59:59,999
Ok, so maintainers:
who is a Debian package maintainer?

27
99:59:59,999 --> 99:59:59,999
Keep your hand up if you
maintain it in 'git'.

28
99:59:59,999 --> 99:59:59,999
Keep your hand up if it's
a 'native' package.

29
99:59:59,999 --> 99:59:59,999
You should be uploading it
with 'dgit'!

30
99:59:59,999 --> 99:59:59,999
[laughter]
You can put your hands down now.

31
99:59:59,999 --> 99:59:59,999
'dgit' will publish your actual
'git' history for all its users

32
99:59:59,999 --> 99:59:59,999
who will then be able to send you
pull requests, patches,

33
99:59:59,999 --> 99:59:59,999
you will be able to 'dgit pull'
any non-maintainer upload (NMU)

34
99:59:59,999 --> 99:59:59,999
you will be able to 'dgit fetch'
the NMU and review it

35
99:59:59,999 --> 99:59:59,999
just like you would with 'git fetch'

36
99:59:59,999 --> 99:59:59,999
even if the uploader didn't use 'dgit'.

37
99:59:59,999 --> 99:59:59,999
So maintainers again:
who is a Debian maintainer?

38
99:59:59,999 --> 99:59:59,999
Who here amongst you generates
your original archives from 'git' somehow?

39
99:59:59,999 --> 99:59:59,999
Maybe with 'pristine-tar', or 'git-buildpackage'
branches, or something?

40
99:59:59,999 --> 99:59:59,999
[anticipatory laughter]

41
99:59:59,999 --> 99:59:59,999
You should be using 'dgit push', because
'dgit' will push your actual patch queue
